class ContestController < ApplicationController
  def show
    @contest = Contest.joins(:game_mechanic).includes(:game_mechanic).find(params[:id])
  end
end
