class GameSessionController < ApplicationController
  def create
    CustomGameSessionWorker.perform_async(current_user.id)
  end
end
