class StrategyController < ApplicationController
  def create
    @source_text = !params[:text].blank? ? params[:text] : params[:file].read
    @strategy = Strategy.new(
      source: @source_text,
      user: current_user,
      version: 0, # TODO: increment version
      contest_id: params[:contest_id]
    )
    @strategy.save
  end
end
