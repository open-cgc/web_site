class Contest < ApplicationRecord
  belongs_to :game_mechanic
  has_many :strategies
end
