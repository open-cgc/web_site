class CustomGameSessionWorker
  include Sidekiq::Worker

  def perform(user_id)
    @user = User.find(user_id)
    logger.info @user.email
  end
end
