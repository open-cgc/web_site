Rails.application.routes.draw do
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  get 'welcome/index'
  root 'welcome#index'
  get '/contests/:id', to: 'contest#show', as: 'contest'
  post 'contests/:contest_id/strategies', to: 'strategy#create', as: :create_strategy
  post 'contests/:contest_id/game_sessions', to: 'game_session#create', as: :create_game_session
end
