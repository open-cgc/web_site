class AddDescriptionToContests < ActiveRecord::Migration[5.1]
  def change
    add_column :contests, :description, :string
  end
end
