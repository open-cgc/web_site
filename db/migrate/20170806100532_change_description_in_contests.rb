class ChangeDescriptionInContests < ActiveRecord::Migration[5.1]
  def change
    change_column :contests, :description, :text
  end
end
