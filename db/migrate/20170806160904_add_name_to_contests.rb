class AddNameToContests < ActiveRecord::Migration[5.1]
  def change
    add_column :contests, :name, :string
  end
end
