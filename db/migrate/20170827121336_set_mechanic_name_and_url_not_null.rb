class SetMechanicNameAndUrlNotNull < ActiveRecord::Migration[5.1]
  def change
    change_column_null :game_mechanics, :name, false
    change_column_null :game_mechanics, :url, false
  end
end
