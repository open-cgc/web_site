class AddGameMechanicRefToContests < ActiveRecord::Migration[5.1]
  def change
    add_reference :contests, :game_mechanic, foreign_key: true
  end
end
