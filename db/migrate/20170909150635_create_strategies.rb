class CreateStrategies < ActiveRecord::Migration[5.1]
  def change
    create_table :strategies do |t|
      t.integer :version
      t.timestamps
    end
    add_reference :strategies, :user, foreign_key: true
    add_reference :strategies, :contest, foreign_key: true
  end
end
