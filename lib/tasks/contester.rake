namespace :contester do
  desc 'Creates game session with random users'
  task random_game_session: :environment do
    @user = User.take
    Rails.logger.info @user.email
  end
end
