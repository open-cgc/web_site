require 'rails_helper'

describe 'Rails_admin authorization', type: :request do
  subject { get '/admin' }

  context 'when user is admin' do
    let!(:user) { create :user, admin: true }

    before do
      sign_in user
      subject
    end

    it 'provides admin interface' do
      expect(response).to_not redirect_to '/'
      expect(response.body).to include('Site Administration')
      expect(response.body).to include(user.email)
    end
  end

  context 'when user is not admin' do
    let!(:user) { create :user }

    before do
      sign_in user
      subject
    end

    it 'redirects to root_url' do
      expect(response).to redirect_to '/'
    end
  end
end
