FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "#{FFaker::Internet.user_name}#{rand(100)}#{n}@example.com" }
    password { FFaker::Internet.password }
    admin false
  end
end
