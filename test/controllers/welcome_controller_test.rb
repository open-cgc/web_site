require 'test_helper'

class WelcomeControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get welcome_index_url
    assert_response :success
    assert_select 'h1', /Соревнования/
    assert_select '.user_signed_in_panel', 0
  end

end
